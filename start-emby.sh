#!/bin/bash

sync
clear

username=your_username
usergroup=your_user_group
mountpoint=/mnt
mov_dir=/mnt/movies
ser_dir=/mnt/series

dev="$(lsblk -fs | grep -i crypto | awk '{print $1}')"
sudo cryptsetup --type luks2 open /dev/"$dev" ext
sleep 2
sync
sudo mount /dev/mapper/ext $mountpoint
sleep 2
sync
sudo chown $username:$usergroup $mov_dir $ser_dir -R
sudo chown 0755 $mov_dir $ser_dir -R
sleep 2
sync
clear

echo " " 
echo "************************"
echo "Media has been mounted:"
echo "************************"
df -hT $mountpoint

size_ser="$(du -sh $ser_dir | awk '{print $1}')"
size_mov="$(du -sh $mov_dir | awk '{print $1}')"
count_mov="$(ls -l $mov_dir | wc -l)"
count_ser="$(ls -l $ser_dir | wc -l)"

echo " "
echo "**********"
echo "Contents:"
echo "**********"
echo "$count_mov movies in $size_mov and $count_ser tv shows in $size_ser"

sudo systemctl start emby-server.service
sleep 2
sync

echo " "
echo "*********************"
echo "Emby is now running:"
echo "*********************"
systemctl status emby-server.service | grep -i active: | cut -c 4-
systemctl status emby-server.service | grep -i "main pid:" | cut -c 2-

echo " "
echo "************************************************"
echo "The UI Accessible on http://$(hostname):9153"
echo "************************************************"

