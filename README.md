# Start Emby

Script to start emby server and automate below tasks:
 - Locate LUKS encrypted partition and unlock it
 - mount the partition under /mnt
 - change permissions so user running emby owns and has permission over media
 - provide same stats as [Emby status script](https://gitlab.com/from_sofia/emby-status)

One should change lines 6-10 as per their needs.
